package com.uk.hsman;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class HttpGetRequest {

	public static void main(String[] args) throws MalformedURLException, IOException {
		String url = "https://www.facebook.com/login.php?login_attempt=1"; 
		String charset = "utf-8";
		
		URLConnection connection = new URL(url).openConnection();
		connection.setRequestProperty("Accept-Charset", charset);
		InputStream response = connection.getInputStream();
		
		try (Scanner scanner = new Scanner(response)) {
		    String responseBody = scanner.useDelimiter("\\A").next();
		    System.out.println(responseBody);
		}
	}

}
